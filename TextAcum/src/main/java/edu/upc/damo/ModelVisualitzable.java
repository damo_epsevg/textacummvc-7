package edu.upc.damo;

/**
 * Created by Josep M on 13/10/2014.
 */
public class ModelVisualitzable implements Vista.Visualitzable
{
    Model model;

    ModelVisualitzable(Model model){
        this.model = model;
    }

    @Override
    public int quants() {
       return model.quants();
    }

    @Override
    public Object elem(int i) {
       return model.elem(i);
    }
}
