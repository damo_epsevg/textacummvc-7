package edu.upc.damo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Josep M on 08/10/2014.
 */
public class Model implements Iterable<CharSequence>{


    private List<CharSequence> dades;


    public Model(){
        dades = new ArrayList<CharSequence>();
    }


    public void afegir(CharSequence s){
        dades.add(s);
    }

    /***
     *
     * @param pos Posicio dins del model; base zero
     */
    public void remove(int pos){
        if (pos>=0 && pos < dades.size())
        {
            dades.remove(pos);
        }
    }

    public void buida(){
        dades = new ArrayList<CharSequence>();
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return dades.listIterator();
    }


    public int quants() {
        return dades.size();
    }

    public Object elem(int i) {
        return dades.get(i);
    }
}
